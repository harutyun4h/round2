﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interview.ServiceModel.Types.ResponceModels
{
    public class WebsiteDataResponse
    {
        public string Url { get; set; }
        public int NumberOfLink { get; set; }
        public int NumberOfImages { get; set; }
        public int NumberOfWords { get; set; }
    }
}
