﻿using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interview.ServiceModel.Types.ORMModels
{
    public class WebsiteData
    {
        [AutoIncrement]
        public int Id { get; set; }
        public string Url { get; set; }
        public int NumberOfLink { get; set; }
        public int NumberOfImages { get; set; }
        public int NumberOfWords { get; set; }
        public long TimeRequested { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
