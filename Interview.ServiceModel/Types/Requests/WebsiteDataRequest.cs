﻿using Interview.ServiceModel.Types.ResponceModels;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interview.ServiceModel.Requests
{
    [Route("/websitedata/{Url}", "GET", Summary = "GET Website Data", Notes = " If your requested websites link has symbols, this endpoint can accept your link with this pattern - /websitedata/Url?Url={link}. If there exists request to the same website during last hour, then you will get same results as in the existing request, if it's not exist, then app will get website data by your request.")]
    public class WebsiteDataRequest : IReturn<WebsiteDataResponse>
    {
        public string Url { get; set; }
    }
}
