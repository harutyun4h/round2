﻿using Interview.ServiceModel.Types.ORMModels;
using Interview.ServiceModel.Types.ResponceModels;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interview.ServiceModel.Types.Requests
{
    [Route("/websitedata/search", "GET", Summary = "GET Filtered Website Data",
        Notes = "If you want to use NumberOfLink or NumberOfImages or NumberOfWords or ResponceTimeRange properties, then you should give range for each property which you want to use. If Url property will be equal or part of the any url string stored in DB, then filter will passed.")]

    public class WebsiteSearchRequest : QueryDb<WebsiteData>, IReturn<List<WebsiteDataResponse>>
    {
        [QueryDbField(Template = "UPPER({Field}) LIKE UPPER({Value})",
                   Field = "Url", ValueFormat = "%{0}%")]
        public string Url { get; set; }

        [QueryDbField(Template = "{Field} BETWEEN {Value1} AND {Value2}",
                Field = "NumberOfLink")]
        public int[] NumberOfLink { get; set; }

        [QueryDbField(Template = "{Field} BETWEEN {Value1} AND {Value2}",
                Field = "NumberOfImages")]
        public int[] NumberOfImages { get; set; }

        [QueryDbField(Template = "{Field} BETWEEN {Value1} AND {Value2}",
                Field = "NumberOfWords")]
        public int[] NumberOfWords { get; set; }

        [QueryDbField(Template = "{Field} BETWEEN {Value1} AND {Value2}",
                Field = "ResponceTimeRange")]
        public int[] ResponceTimeRange { get; set; }
    }
}
