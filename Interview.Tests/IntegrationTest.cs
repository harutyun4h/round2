﻿using Funq;
using ServiceStack;
using NUnit.Framework;
using Interview.ServiceInterface;
using Interview.ServiceModel;
using Interview.ServiceModel.Requests;
using ServiceStack.OrmLite;
using ServiceStack.Data;
using Interview.ServiceInterface.Scraping;
using Interview.ServiceModel.Types.Requests;
using System.Collections.Generic;
using Interview.ServiceModel.Types.ResponceModels;
using System.Linq;

namespace Interview.Tests
{
    public class IntegrationTest
    {
        const string BaseUri = "http://localhost:2000/";
        private readonly ServiceStackHost appHost;

        public class AppHost : AppSelfHostBase
        {
            public AppHost() : base("Interview", typeof(MyServices).Assembly) { }

            // Configure your AppHost with the necessary configuration and dependencies your App needs
            public override void Configure(Container container)
            {
               
                Plugins.Add(new AutoQueryFeature { MaxLimit = 100 });

                container.Register<IDbConnectionFactory>(
                  new OrmLiteConnectionFactory(":memory:", SqliteDialect.Provider));
                container.RegisterAutoWiredAs<DataScraper, IDataScraper>().ReusedWithin(ReuseScope.Request);
                Seed();
            }

            private void Seed()
            {
                var dbFactory = HostContext.TryResolve<IDbConnectionFactory>();
                new SeedDatabase().Init(dbFactory);

            }
        }

        public IntegrationTest()
        {
            appHost = new AppHost()
                .Init()
                .Start(BaseUri);
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => appHost.Dispose();

        public IServiceClient CreateClient() => new JsonServiceClient(BaseUri);

        [Test]
        public void Can_call_WebsiteDataService()
        {
            var client = CreateClient();

            var response = client.Get(new WebsiteDataRequest { Url = "test.com" });

            Assert.That(response.Url, Is.EqualTo("test.com"));
        }
        [Test]
        public void Can_call_WebsiteSearchService()
        {
            var client = CreateClient();

            var response = client.Get<List<WebsiteDataResponse>>(new WebsiteSearchRequest { Url= "10Websit" });

            Assert.That(response.SingleOrDefault().Url, Is.EqualTo("10Website.com"));

            response = client.Get<List<WebsiteDataResponse>>(new WebsiteSearchRequest { NumberOfWords=new int[2] { 20,44}, NumberOfLink=new int[2] { 3, 30} }); // should return from 20 to 30

            Assert.That(response.Count(), Is.EqualTo(11));

            response = client.Get<List<WebsiteDataResponse>>(new WebsiteSearchRequest { Url = null });

            Assert.That(response.Count(), Is.GreaterThanOrEqualTo(100)); //SeedDatabase.Init() method adds 100 records

        }
    }
}