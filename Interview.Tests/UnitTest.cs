﻿using NUnit.Framework;
using ServiceStack;
using ServiceStack.Testing;
using Interview.ServiceInterface;
using Interview.ServiceModel;
using Interview.ServiceModel.Types.ResponceModels;
using System.Threading.Tasks;
using Interview.ServiceModel.Types.Requests;
using Interview.ServiceModel.Requests;
using Interview.ServiceInterface.Scraping;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using Funq;
using Interview.ServiceModel.Types.ORMModels;
using System;
using ServiceStack.Host;

namespace Interview.Tests
{
    public class UnitTest
    {
        private readonly ServiceStackHost appHost;


        public UnitTest()
        {
            appHost = new BasicAppHost() {
                ConfigureAppHost = host =>
                {
                    host.Plugins.Add(new AutoQueryFeature { MaxLimit = 100 });
                }
            }.Init();

            var container = appHost.Container;
            container.Register<IDbConnectionFactory>(
              new OrmLiteConnectionFactory(":memory:", SqliteDialect.Provider));

            container.RegisterAutoWiredAs<DataScraper, IDataScraper>().ReusedWithin(ReuseScope.Request);

            container.RegisterAutoWired<WebsiteDataService>();
            new SeedDatabase().Init(container.Resolve<IDbConnectionFactory>());
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => appHost.Dispose();

        [Test]
        public async Task Can_call_WebsiteDataRequest()
        {
            var service = appHost.Container.Resolve<WebsiteDataService>();
            
            var response = await service.Get(new WebsiteDataRequest { Url = "https://docs.servicestack.net/openapi" });

            Assert.That(response.Url, Is.EqualTo("https://docs.servicestack.net/openapi"));

            response = await service.Get(new WebsiteDataRequest { Url = null });

            Assert.That(response.Url, Is.EqualTo(null));
        }
        [Test]
        public void Can_call_WebsiteSearchRequest()
        {
            var service = appHost.Container.Resolve<WebsiteDataService>();
            service.Request = new BasicRequest();
            var response =  service.Get(new WebsiteSearchRequest() { NumberOfLink = new int[2] { 1, 33 } } );

            Assert.That(response.Count, Is.EqualTo(33));
            response = service.Get(new WebsiteSearchRequest() { NumberOfLink = new int[2] { 1, 33 }, NumberOfWords = new int[2] { 2, 23 }, });

            Assert.That(response.Count, Is.EqualTo(22));
        }
    }
}
