﻿using Interview.ServiceModel.Types.ORMModels;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interview.ServiceInterface
{
    public class SeedDatabase
    {
        public void Init(IDbConnectionFactory dbFactory)
        {
            using (var db = dbFactory.Open())
            {
                if (db.TableExists<WebsiteData>())
                {
                    return;
                }
                db.CreateTable<WebsiteData>();
                for (int i = 0; i < 100; i++)
                {
                    db.Insert(new WebsiteData()
                    {
                        Url = i + "Website.com",
                        NumberOfLink = i,
                        NumberOfImages = i,
                        NumberOfWords = i,
                        TimeRequested = i,
                        CreatedDate = DateTime.UtcNow
                    });
                }
            }
        }
    }
}
