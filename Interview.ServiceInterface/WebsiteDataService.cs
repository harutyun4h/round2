﻿using Interview.ServiceInterface.Scraping;
using Interview.ServiceModel;
using Interview.ServiceModel.Requests;
using Interview.ServiceModel.Types.ORMModels;
using Interview.ServiceModel.Types.Requests;
using Interview.ServiceModel.Types.ResponceModels;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interview.ServiceInterface
{
    public class WebsiteDataService : Service
    {
        private readonly IDataScraper _scraper;
        private readonly IDbConnectionFactory _connectionFactory;
        public IAutoQueryDb AutoQuery { get; set; }
        public WebsiteDataService(IDataScraper scraper, IDbConnectionFactory connectionFactory)
        {
            _scraper = scraper;
            _connectionFactory = connectionFactory;
        }

        public async Task<WebsiteDataResponse> Get(WebsiteDataRequest request)
        {
            if (string.IsNullOrEmpty(request.Url))
            {
                return new WebsiteDataResponse();
            }

            WebsiteData websiteData=null;
            using (var _connection = _connectionFactory.Open())
            {
                websiteData = _connection.Select<WebsiteData>(x => x.Url.ToUpper().Equals(request.Url.ToUpper()) && x.CreatedDate >= DateTime.UtcNow.AddHours(-1)).FirstOrDefault();
                
            }
            if (websiteData == null)
            {
                var watch = new Stopwatch();
                watch.Start();
                var document = await _scraper.GetDocument(request.Url);
                watch.Stop();
                websiteData = new WebsiteData()
                {
                    Url = request.Url,
                    NumberOfLink = _scraper.CountLinks(document),
                    NumberOfImages = _scraper.CountImages(document),
                    NumberOfWords = _scraper.CountWords(document),
                    TimeRequested = watch.ElapsedMilliseconds,
                    CreatedDate = DateTime.UtcNow

                };
                var tableContent = new List<WebsiteData>();
                using (var _connection = _connectionFactory.Open())
                {
                    _connection.Insert(websiteData);
                    tableContent = _connection.Select<WebsiteData>();
                }

            }

            return new WebsiteDataResponse()
            {
                Url = request.Url,
                NumberOfLink = websiteData.NumberOfLink,
                NumberOfImages = websiteData.NumberOfImages,
                NumberOfWords = websiteData.NumberOfWords
            };
        }

        public List<WebsiteDataResponse> Get(WebsiteSearchRequest request)
        {
            var q = AutoQuery.CreateQuery(request, base.Request);
            var data = AutoQuery.Execute(request, q);

            var responce = data.Results.Select(d => new WebsiteDataResponse {
                Url = d.Url,
                NumberOfLink = d.NumberOfLink,
                NumberOfImages = d.NumberOfImages,
                NumberOfWords = d.NumberOfWords
            }).ToList();
            return responce;
        }
    }
}
